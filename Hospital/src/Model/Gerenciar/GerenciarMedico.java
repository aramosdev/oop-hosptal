/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Gerenciar;

import Model.Agenda.Agendamento;
import Model.Pessoa.Medico;
import java.util.List;

/**
 *
 * @author aramos
 */
public class GerenciarMedico {

    private List<Medico> medicos;

    public GerenciarMedico() {
        medicos = (List<Medico>) ManagerXML.readMedico();
    }

    public List<Medico> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<Medico> medicos) {
        this.medicos = medicos;
    }

    public String cadastroMedico(Medico medico) {
        if ((medico.getCrm().isEmpty()) && (medico.getCpf().isEmpty())) return "POR FAVOR, INFORME O CRM/CPF";
        int index = consultMedicoCpf(medico.getCpf());
        if (index !=-1) if(medico.getCpf().equalsIgnoreCase(medicos.get(index).getCpf()))return "MEDICO JÁ CADASTRADO";

        medicos.add(medico);
        ManagerXML.saveMedico(medicos);
        return "CADASTRO REALIZADO COM SUCESSO!";
    }
    
    public int consultMedicoCpf(String _cpf) {
        int medicoSize = medicos.size();
        for (int i = 0; i < medicoSize; i++) {
            Medico m = medicos.get(i);
            if (m.getCpf().equalsIgnoreCase(_cpf)) {
                return i;
            }
        }
        return -1;
    }
    
    public String deleteMedico( Medico m ){
        Agendamento a = new Agendamento();
        a.deleteConsultMedico(m);
        medicos.remove(m);
        ManagerXML.saveMedico(medicos);
        return "MEDICO EXCLUIDO COM SUCESSO!";
    }
     public String changeMedico( Medico medico ){
        if ((medico.getNome().isEmpty())) {
            return "POR FAVOR, INFORME o NOME DO PACIENTE";
        }
        ManagerXML.saveMedico(medicos);
        return "ALTERAÇÃO REALIZADA COM SUCESSO!";
    }
}

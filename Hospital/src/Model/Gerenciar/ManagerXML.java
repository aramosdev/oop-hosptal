
package Model.Gerenciar;


import Model.Agenda.CreateCalendar;
import Model.Agenda.Dia;
import Model.Agenda.Mes;
import Model.Agenda.Semana;
import Model.Leito.GerenciaQuarto;
import Model.Leito.Quarto;
import Model.Pessoa.Funcionarios;
import Model.Pessoa.Medico;
import Model.Pessoa.Paciente;
import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManagerXML {

    static String relativePath;
    static XStream xStream = new XStream();
    public static final String XML_gerencia_paciente = "paciente_registration";
    public static final String XML_gerenciar_funcionarios = "gerenciar_funcionario";
    public static final String XML_GerenciarMedico = "gerenciar_medico";
    public static final String XML_CreateCalendar = "agenda_consulta";
    public static final String XML_GereciaQuartos = "gerencia_quartos";

    /**
     *@see  Inicializa o xml e as classes
     */
    static public void initialize() {

        xStream.alias(XML_gerencia_paciente, Paciente.class);//nome do xStream para cliente
        xStream.alias(XML_gerenciar_funcionarios, Funcionarios.class);//nome do xStream para os
        xStream.alias(XML_GerenciarMedico, Medico.class);//nome do xStream para os
        xStream.alias(XML_CreateCalendar, CreateCalendar.class);
        xStream.alias(XML_GereciaQuartos, GerenciaQuarto.class);

        String path = ManagerXML.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        System.out.println(path);//Pegando o caminho relativo
        try {
            relativePath = URLDecoder.decode(path, "UTF-8");
            int lastSlash = relativePath.lastIndexOf("/");
            relativePath = relativePath.substring(0, lastSlash + 1);
            System.out.println("Relative path: " + relativePath);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ManagerXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     * @param Clientes
     */
    static public void savePaciente(List<?> paciente) {
        saveXml(XML_gerencia_paciente, paciente);
    }
    /**
     * 
     * @param  
     */
    static public void saveMedico(List<?> medicos){
        saveXml(XML_GerenciarMedico, medicos);
    }
    /**
     * 
     * @param fucionarios 
     */
    static public void saveFuncionario(List<?> fucionarios){
        saveXml(XML_gerenciar_funcionarios, fucionarios);
    }
    /**
     * 
     * @param agenda 
     */
    static public void saveAgenda(List<?> agenda){
        saveXml(XML_CreateCalendar, agenda);
    }
    
    /**
     * 
     * @param quartos 
     */
    static public void saveQuarto(List<?> quartos){
        saveXml(XML_GereciaQuartos, quartos);
    }
    /**
     * 
     * @return Lista de clientes
     */
    static public List<?> readPaciente(){
        return (List<?>) xStream.fromXML(fileRead(XML_gerencia_paciente));
    }/**
     * 
     * @return Lista de Os 
     */
    static public List<?> readMedico(){
        return (List<?>) xStream.fromXML(fileRead(XML_GerenciarMedico));
    }
     /**
      * 
      * @return 
      */
    static public List<?> readFuncionario(){
        return (List<?>) xStream.fromXML(fileRead(XML_gerenciar_funcionarios));
    }
    
    /**
     * 
     * @param list
     * @return 
     */
    static public List readAgenda(String list){
        switch(list){
            case "dia":
                return  (List<Dia>) xStream.fromXML(fileRead(XML_CreateCalendar));
            case "mes":
                return  (List<Mes>) xStream.fromXML(fileRead(XML_CreateCalendar));
            case "semana":
                return  (List<Semana>) xStream.fromXML(fileRead(XML_CreateCalendar));
        }
        return null;
    }
    
    /**
     * 
     * @return 
     */
    static public List<?> readQuartos(){
        return (List<?>) xStream.fromXML(fileRead(XML_GereciaQuartos));
    }
    
    
    /**
     * 
     * @param fileName
     * @param data 
     */
    static private void saveXml(String fileName, Object data) {
        
        try {
            FileOutputStream outputStream = new FileOutputStream(relativePath + fileName + ".xml");
            Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
            xStream.toXML(data, writer);
        } catch (Exception e) {
            System.out.println("Erro ao gravar o XML: " + e);
        }

    }

    static private File fileRead(String name) {
        File file = new File(relativePath + name + ".xml");
        if (!file.isFile()) {
            try {
                FileWriter w = new FileWriter(relativePath + name + ".xml");
                w.write("<list></list>");
                w.close();
                file = new File(relativePath + name + ".xml");
                return file;

            } catch (IOException ex) {
                Logger.getLogger(ManagerXML.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return file;
    }

}

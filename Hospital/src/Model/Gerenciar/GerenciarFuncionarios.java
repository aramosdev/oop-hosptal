/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Gerenciar;

import Model.Agenda.Agendamento;
import Model.Pessoa.Funcionarios;
import java.util.List;

/**
 *
 * @author alberto
 */
public class GerenciarFuncionarios {

    private List<Funcionarios> funcionarios;


    public GerenciarFuncionarios() {
        funcionarios = (List<Funcionarios>) ManagerXML.readFuncionario();
    }

    public List<Funcionarios> getFuncionarios() {
        return funcionarios;
    }

    public String cadastroFuncionarios(Funcionarios funcionario) {
        if ((funcionario.getDeptAtuacao().isEmpty()) || (funcionario.getCpf().isEmpty())) return "POR FAVOR, INFORME O DEPTATUAÇÃO/CPF";
        int index = queryFuncionario(funcionario.getCpf());
        if (index != -1 ) if (funcionario.getCpf().equalsIgnoreCase(funcionarios.get(index).getCpf())) return "FUNCIONARIO JÁ CADASTRADO.";
        
        funcionarios.add(funcionario);
        
        ManagerXML.saveFuncionario(funcionarios);
        return "CADASTRO REALIZADO COM SUCESSO!";
    }
    
    public int queryFuncionario(String _cpf){
        int sizeList = funcionarios.size();
        for (int i = 0; i < sizeList; i++) {
            Funcionarios f = funcionarios.get(i);
            if (f.getCpf().equalsIgnoreCase(_cpf))return i;
        }
        return-1; 
    }
    
    public String deleteFuncionario( Funcionarios f ){
        Agendamento a = new Agendamento();
        funcionarios.remove(f);
        ManagerXML.saveFuncionario(funcionarios);
        return "FUNCIONARIO EXCLUIDO COM SUCESSO!";
    }
    
    public String changeFuncionario( Funcionarios funcionario ){
        if ( ( funcionario.getNome().isEmpty() ) || ( funcionario.getDeptAtuacao().isEmpty()) ) {
            return "POR FAVOR, INFORME o NOME DO FUNCIONARIO";
        }
        ManagerXML.saveFuncionario(funcionarios);
        return "ALTERAÇÃO REALIZADA COM SUCESSO!";
    }
    
}

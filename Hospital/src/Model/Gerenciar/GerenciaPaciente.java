/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Gerenciar;

import Model.Agenda.Agendamento;
import Model.Pessoa.Paciente;
import java.util.List;

/**
 *
 * @author alberto
 */
public class GerenciaPaciente {

    private List<Paciente> pacientes;

    public GerenciaPaciente() {
        pacientes = (List<Paciente>) ManagerXML.readPaciente();
    }

    public String cadastroPaciente(Paciente paciente) {
        if ((paciente.getId() < 0) || (paciente.getCpf().isEmpty())) {
            return "POR FAVOR, INFORME O id/CPF";
        }
        Paciente p = consultPaciente(paciente.getCpf());
        if(p != null) if(p.getCpf().equalsIgnoreCase(paciente.getCpf()) ) return "PACIENTE JÁ CADASTRADO."; 
        
        pacientes.add(paciente);
        
        ManagerXML.savePaciente(pacientes);
        return "CADASTRO REALIZADO COM SUCESSO!";
                
    }
    public Paciente consultPaciente(String _cpf){
        int pacienteSize = pacientes.size();
        for (int i = 0; i < pacienteSize; i++) {
            Paciente p = pacientes.get(i);
            if (p.getCpf().equalsIgnoreCase(_cpf)) {
                return p;
            }
        }
        return null;
    }
    
    public String deletePaciente( Paciente p ){
        Agendamento a = new Agendamento();
        a.deleteConsultPaciente(p);
        pacientes.remove(p);
        ManagerXML.savePaciente(pacientes);
        return "PACIENTE EXCLUIDO COM SUCESSO!";
    }
    public String changePaciente( Paciente paciente ){
        if ((paciente.getNome().isEmpty())) {
            return "POR FAVOR, INFORME o NOME DO PACIENTE";
        }
        ManagerXML.savePaciente(pacientes);
        return "ALTERAÇÃO REALIZADA COM SUCESSO!";
    }
}

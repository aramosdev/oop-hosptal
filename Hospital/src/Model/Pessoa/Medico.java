/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Pessoa;

/**
 *
 * @author Wellington
 */
public class Medico extends Pessoa {

    private String crm;
    
    public Medico() {
    }
    
    public Medico(String crm, String nome, int idade, String cpf, String endereço, String telefone, String email) {
        super(nome, idade, cpf, endereço, telefone, email);
        this.crm = crm;
    }
    
    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    @Override
    public String toString() {
        return "Crm: " + crm + "\n" + super.toString();
    }
   
}

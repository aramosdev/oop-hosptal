/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Pessoa;

/**
 *
 * @author Wellington
 */
public class Funcionarios extends Pessoa{
    
    private String deptAtuacao;

    public Funcionarios(String deptAtuacao, String nome, int idade, String cpf, String endereço, String telefone, String email) {
        super(nome, idade, cpf, endereço, telefone, email);
        this.deptAtuacao = deptAtuacao;
    }
    
    
    public String getDeptAtuacao() {
        return deptAtuacao;
    }

    public void setDeptAtuacao(String deptAtuacao) {
        this.deptAtuacao = deptAtuacao;
    }

    @Override
    public String toString() {
        return super.toString() +"\n"+ "Departamento de Atuacao: " + deptAtuacao;
    }
   
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Leito;


import Model.Pessoa.Paciente;

/**
 *
 * @author aramos
 */
public class Leito {
    private String cod;   
    private Paciente paciente;

    public Leito() {
    }

    public Leito(String cod) {
        this.cod = cod;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

}

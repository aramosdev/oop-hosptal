/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Leito;

import Model.Gerenciar.ManagerXML;
import Model.Pessoa.Paciente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aramos
 */
public class GerenciaQuarto {

    private List<Quarto> quartos;

    public GerenciaQuarto() {
        quartos = (List<Quarto>) ManagerXML.readQuartos();


    }

    public List<Quarto> getQuartos() {
        return quartos;
    }

    public String saveQuarto(Quarto _q) {

        if (queryRoom(_q.getId()) != null) {
            return "QUARTO JÁ CADASTRADO";
        }

        quartos.add(_q);
        ManagerXML.saveQuarto(quartos);

        return "CADASTRO REALIZADO COM SUCESSO!";
    }

    public Quarto queryRoom(int _id) {
        for (Quarto quarto : quartos) {
            if (quarto.getId() == _id) {
                return quarto;
            }
        }
        return null;
    }

    public String saveLeito(Leito _l, int _id) {

        ArrayList<Leito> al = new ArrayList<>();
        Leito l = queryLeito(_l.getCod());
        if (l != null) {
            return "LEITO JÁ CADASTRADO";
        }
        Quarto q = queryRoom(_id);
        if (q == null) {
            return "QUARTO NÃO CADASTRADO!";
        }
        for (Quarto quarto : quartos) {
            if (_id == quarto.getId()) {
                if (quarto.getLeito() != null) {
                    for (Leito leito : quarto.getLeito()) {
                        al.add(leito);                
                    }
                }
                al.add(_l);
                quarto.setLeito(al);
            }
        }
        if ((q.getLeito().size() > q.getNumberMaxLeitos())) {
            return "NÃO HA MAIS LEITOS DISPONIVEIS";
        }

        ManagerXML.saveQuarto(quartos);
        return "CADASTRO REALIZADO COM SUCESSO!";
    }

    public Leito queryLeito(String _cod) {
        for (Quarto quarto : quartos) {
            if (quarto.getLeito() != null) {
                for (Leito leito : quarto.getLeito()) {
                    if (leito.getCod().equalsIgnoreCase(_cod)) {
                        return leito;
                    }
                }
            }
        }
        return null;
    }

    public String saveRegisterLeito(int _id, String _cod, Paciente p) {
        Quarto q = queryRoom(_id);
        if (q == null) {
            return "QUARTO NÃO CONSTA NO SITEMA";
        }



        return null;
    }
}

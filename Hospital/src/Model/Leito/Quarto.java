/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Leito;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author aramos
 */
public class Quarto {
    private int id;
    private int numberMaxLeitos;
    private List<Leito> leito ;

    public Quarto(int id, int numberMaxLeitos) {
        this.id = id;
        this.numberMaxLeitos = numberMaxLeitos;
    }

    public Quarto() {
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getNumberMaxLeitos() {
        return numberMaxLeitos;
    }

    public void setNumberMaxLeitos(int numberMaxLeitos) {
        this.numberMaxLeitos = numberMaxLeitos;
    }

    public List<Leito> getLeito() {
        return leito;
    }

    public void setLeito(List<Leito> leito) {
        this.leito = leito;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Agenda;

import Model.Gerenciar.ManagerXML;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author aramos
 */
public class CreateCalendar {

    private List<Dia> dias;
    private List<Mes> mes;
    private List<Semana> semanas;

    public CreateCalendar() {
        this.mes = ManagerXML.readAgenda("mes");
        this.dias = new ArrayList<>();
        this.semanas = new ArrayList<>();
         
    }
    
    public void InitCalendar(int _dia, int _mes, Periodo _periodos, int _index) {
        int ano = 2013;
        createDia(_dia, _mes, _periodos);
        createSemana(_dia, _mes, ano);
        createMes(_mes);
        ManagerXML.saveAgenda(mes);
    }

    public void createDia(int _dia, int _mes, Periodo _periodos) {
        int ano = 2013;
        String diaSemana = dias(ano, _mes, _dia);
        Dia dia = new Dia(_dia, diaSemana, _periodos);
        dias.add(dia);
        
    }

    public void createSemana(int _dia, int mes, int _ano) {
        int ano = 2013;
        int wek = semana(ano, mes, _dia);
        Semana s = new Semana(wek, dias);
        semanas.add(s);

    }

    public void createMes(int _mes) {
        Mes m = new Mes(semanas, _mes);
        mes.add(m);

    }

    public String dias(int _ano, int _mes, int _dia) {
        Calendar calendario = new GregorianCalendar(_ano, _mes - 1, _dia);
        int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);

        return pesquisarDiaSemana(diaSemana);

    }
    //faz a pesquisa, dado um inteiro de 1 a 7 

    public String pesquisarDiaSemana(int _diaSemana) {
        String diaSemana = null;

        switch (_diaSemana) {

            case 1: {
                diaSemana = "Domingo";
                break;
            }
            case 2: {
                diaSemana = "Segunda";
                break;
            }
            case 3: {
                diaSemana = "Terça";
                break;
            }
            case 4: {
                diaSemana = "Quarta";
                break;
            }
            case 5: {
                diaSemana = "Quinta";
                break;
            }
            case 6: {
                diaSemana = "Sexta";
                break;
            }
            case 7: {
                diaSemana = "Sábado";
                break;
            }

        }

        return diaSemana;

    }

    public int semana(int _ano, int _mes, int _dia) {
        Calendar calendario = new GregorianCalendar(_ano, _mes, _dia);
        int semana = calendario.get(Calendar.DAY_OF_WEEK_IN_MONTH);

        return semana;
    }

    public boolean checkDia(int _dia) {
        int semanaSize = semanas.size();
        for (int i = 0; i < semanaSize; i++) {
            if (semanas.get(i).getDias().get(i).getData() == _dia) {
                return false;
            }
        }
        return true;
    }

    public int checkMesAtual() {
        Calendar cal = GregorianCalendar.getInstance();
        int mesAtual = cal.get(Calendar.MONTH);

        return mesAtual;
    }
}

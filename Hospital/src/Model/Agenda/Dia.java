/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Agenda;

/**
 *
 * @author aramos
 */
public class Dia {
    private int data;
    private String diaSemana;
    private Periodo periodo;

    public Dia(int data, String diaSemana, Periodo periodo) {
        this.data = data;
        this.diaSemana = diaSemana;
        this.periodo = periodo;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    
    
}

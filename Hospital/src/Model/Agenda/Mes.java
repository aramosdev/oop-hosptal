/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Agenda;

import java.util.List;

/**
 *
 * @author aramos
 */
public class Mes {
    private List<Semana> semana;
    private int numero;

    public Mes(List<Semana> semana, int numero) {
        this.semana = semana;
        this.numero = numero;
    }

    public List<Semana> getSemana() {
        return semana;
    }

    public void setSemana(List<Semana> semana) {
        this.semana = semana;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
}

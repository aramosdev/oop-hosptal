/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Agenda;

import java.util.List;

/**
 *
 * @author aramos
 */
public class Semana {
    private int numeroSemana;
    private List<Dia> dias;

    public Semana(int numeroSemana, List<Dia> dias) {
        this.numeroSemana = numeroSemana;
        this.dias = dias;
    }

    public int getNumeroSemana() {
        return numeroSemana;
    }

    public void setNumeroSemana(int numeroSemana) {
        this.numeroSemana = numeroSemana;
    }

    public List<Dia> getDias() {
        return dias;
    }

    public void setDias(List<Dia> dias) {
        this.dias = dias;
    }
    
    
}

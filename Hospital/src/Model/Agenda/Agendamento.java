/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Agenda;

import Model.Gerenciar.GerenciaPaciente;
import Model.Gerenciar.GerenciarMedico;
import Model.Gerenciar.ManagerXML;
import Model.Pessoa.Medico;
import Model.Pessoa.Paciente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aramos
 */
public class Agendamento {

    private GerenciarMedico medico;
    private GerenciaPaciente paciente;

    public String agendaConsulta(int _dia, int _mes, String _cpfMedico, String _cpfPaciente, int _periodo) {
        medico = new GerenciarMedico();
        paciente = new GerenciaPaciente();
        int index = medico.consultMedicoCpf(_cpfMedico);
        Paciente pi = paciente.consultPaciente(_cpfPaciente);
        CreateCalendar calendar = new CreateCalendar();
        String diaSemana = calendar.dias(2013, _mes, _dia);
        String type = null;

        if (index == -1) {
            return "O MÉDICO NÃO CONSTA NOS REGISTROS";
        }
        if (pi == null) {
            return "O PACIENTE NÃO CONSTA NOS REGISTROS";
        }
        if (_mes > 12) {
            return "INFORME UM MES VALIDO";
        }
        if (_dia > 31) {
            return "INFORME UM DIA VALIDO";
        }
        if (_mes < calendar.checkMesAtual()) {
            return "INFORME UM MES VALIDO";
        }

        switch (_periodo) {
            case 1:
                type = "Manha";
                break;
            case 2:
                type = "Tarde";
                break;
            case 3:
                type = "Noite";
                break;
            default:
                return "INFORME UM PERIODO VALIDO;";
        }
        if ((diaSemana.equalsIgnoreCase("Sábado") || (diaSemana.equalsIgnoreCase("Domingo")))) {
            return "DIA NÃO DISPONIVEL PARA CONSULTA!";
        }
        if (consultPeriodoLivre(_dia, _mes, type, index)) {
            return "NÃO HÁ DISPONIBILIDADE!";
        }

        Periodo p = new Periodo(type, pi, medico.getMedicos().get(index));

        calendar.InitCalendar(_dia, _mes, p, index);

        return "CONSULTA AGENDADA COM SUCESSO!";
    }

    public boolean consultPeriodoLivre(int _dia, int _mes, String _type, int _index) {
        Medico m = medico.getMedicos().get(_index);
        List<Mes> mes = ManagerXML.readAgenda("mes");
        int listMes = mes.size();
        if (mes != null) {
            for (int i = 0; i < listMes; i++) {
                Mes me = mes.get(i);
                if (_mes != me.getNumero()) {
                    return false;
                }
                for (int j = 0; j < me.getSemana().size(); j++) {
                    Semana s = me.getSemana().get(j);
                    for (int k = 0; k < s.getDias().size(); k++) {
                        Dia d = s.getDias().get(k);
                        int dia = d.getData();
                        if (dia == _dia) {
                            if (d.getPeriodo().getMedico().getCpf().equalsIgnoreCase(m.getCpf())) {
                                if (d.getPeriodo().getHorario().equalsIgnoreCase(_type)) {
                                    return true;
                                }
                            }
                        }
                    }

                }
            }
        }
        return false;
    }

    public boolean deleteConsultPaciente(Paciente p) {
        List<Mes> mes = ManagerXML.readAgenda("mes");
        List<Mes> diaAux = new ArrayList<>();
        int listMes = mes.size();
        if (mes != null) {
            for (Mes mes1 : mes) {
                for (Semana semana : mes1.getSemana()) {
                    for (Dia dia : semana.getDias()) {
                        if (dia.getPeriodo().getPaciente().getCpf().equalsIgnoreCase(p.getCpf())) {
                            diaAux.add(mes1);
                        }
                    }
                }
            }
            for (Mes mes2 : diaAux) {
                mes.remove(mes2);
            }
            ManagerXML.saveAgenda(mes);
            return true;
        }else{
            return false;
        }
    }
    
    public boolean deleteConsultMedico(Medico m) {
        List<Mes> mes = ManagerXML.readAgenda("mes");
        List<Mes> diaAux = new ArrayList<>();
        int listMes = mes.size();
        if (mes != null) {
            for (Mes mes1 : mes) {
                for (Semana semana : mes1.getSemana()) {
                    for (Dia dia : semana.getDias()) {
                        if (dia.getPeriodo().getMedico().getCpf().equalsIgnoreCase(m.getCpf())) {
                            diaAux.add(mes1);
                        }
                    }
                }
            }
            for (Mes mes2 : diaAux) {
                mes.remove(mes2);
            }
            ManagerXML.saveAgenda(mes);
            return true;
        }else{
            return false;
        }
    }
    
    public boolean deleteConsult(int _dia, String type){
        List<Mes> mes = ManagerXML.readAgenda("mes");
        List<Mes> diaAux = new ArrayList<>();
        int listMes = mes.size();
        if (mes != null) {
            for (Mes mes1 : mes) {
                for (Semana semana : mes1.getSemana()) {
                    for (Dia dia : semana.getDias()) {
                        if (dia.getData()==_dia || dia.getPeriodo().getHorario().equalsIgnoreCase(type)) {
                            diaAux.add(mes1);
                        }
                    }
                }
            }
            for (Mes mes2 : diaAux) {
                mes.remove(mes2);
            }
            ManagerXML.saveAgenda(mes);
            return true;
        }else{
            return false;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerInicial;
import java.util.Scanner;

/**
 *
 * @author alberto
 */
public class TelaInicial {

    private ControllerInicial control;

    public TelaInicial(ControllerInicial control) {
        this.control = control;
    }

    public void MenuInicial() {

        String input;
        int opcao = -1;
        Scanner scan = new Scanner(System.in);

        while (opcao != 0) {

            System.out.println("== HOSPITAL SABOIA ==");
            System.out.println("1. Cadastrar Pacientes.");
            System.out.println("2. Cadastrar Médicos.");
            System.out.println("3. Agendar Consulta.");
            System.out.println("4. Cadastrar Funcionario.");
            System.out.println("5. Cadastrar Quartos.");
            System.out.println("0. Sair.");

            System.out.println("Digite a opcao desejada:");
            input = scan.nextLine();
            opcao = Integer.parseInt(input);

            switch (opcao) {
                case 1:
                    control.callPatient();
                    break;

                case 2:
                    control.callMedic();
                    break;

                case 3:
                    control.callAgenda();
                    break;
                case 4:
                    control.callFuncionarios();
                    break;
                case 5:
                    control.callQuartos();
                    break;

            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import java.util.Scanner;
import Controller.ControllerAgenda;

/**
 *
 * @author aramos
 */
public class TelaAgenda {

    private ControllerAgenda comAgenda;
    private Scanner scan = new Scanner(System.in);

    public TelaAgenda(ControllerAgenda c) {
        comAgenda = c;
    }

    public void agenda() {


        try {
            int dia;
            int mes;
            int periodo = 4;
            String input;
            String cpfPaciente;
            String cpfMedico;
            System.out.println("== Agendar Consultas ==");
            System.out.printf("Informe o dia para consulta: ");
            input = scan.nextLine();
            dia = Integer.parseInt(input);
            System.out.printf("Informe o mes para consulta: ");
            input = scan.nextLine();
            mes = Integer.parseInt(input);
            while (periodo == 4) {

                System.out.println("== Selecione o periodo ==");
                System.out.println("1. Manha.");
                System.out.println("2. Tarde.");
                System.out.println("3. Noite.");

                System.out.println("Digite a opcao desejada:");
                input = scan.nextLine();
                periodo = Integer.parseInt(input);

                switch (periodo) {
                    case 1:
                        periodo = 1;
                        break;

                    case 2:
                        periodo = 2;
                        break;

                    case 3:
                        periodo = 3;
                        break;

                }
            }
            System.out.printf("Informe o cpf do paciente: ");
            cpfPaciente = scan.nextLine();
            System.out.printf("Informe o cpf do média a atender a consulta: ");
            cpfMedico = scan.nextLine();

            String result = comAgenda.agendarConsulta(dia, mes, periodo, cpfMedico, cpfPaciente);
            System.out.println(result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void removeConslut() {
        try {
            int dia;
            int mes;
            int periodo = 4;
            String type = null;
            String input;
            System.out.printf("Informe o dia para excluir a consulta: ");
            input = scan.nextLine();
            dia = Integer.parseInt(input);
            while (periodo == 4) {

                System.out.println("== Selecione o periodo a ser excluido ==");
                System.out.println("1. Manha.");
                System.out.println("2. Tarde.");
                System.out.println("3. Noite.");

                System.out.println("Digite a opcao desejada:");
                input = scan.nextLine();
                periodo = Integer.parseInt(input);

                switch (periodo) {
                    case 1:
                        type = "Manha";
                        periodo = 1;
                        break;

                    case 2:
                        type = "Tarde";
                        periodo = 2;
                        break;

                    case 3:
                        type = "Noite";
                        periodo = 3;
                        break;

                }
            }
            
            String result = comAgenda.deleteConsult(dia, type);
            System.out.println(result);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void telaAgenda() {

        String input;
        int opcao = -1;
        while (opcao != 0) {
            try {
                System.out.println("== Agenda de Consulta ==");
                System.out.println("1. Agenda Consulta.");
                System.out.println("2. Exclui Consulta");
                System.out.println("0. Voltar");


                System.out.println("Digite a opcao desejada:");

                input = scan.nextLine();
                opcao = Integer.parseInt(input);

            } catch (Exception e) {
                System.err.println(e.getMessage());
            }

            switch (opcao) {
                case 1:
                    agenda();
                    break;
                case 2:
                    removeConslut();
                    break;
            }
        }
    }
}

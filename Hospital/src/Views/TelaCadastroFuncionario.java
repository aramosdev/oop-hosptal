/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerFuncionarios;
import java.util.Scanner;

/**
 *
 * @author alberto
 */
public class TelaCadastroFuncionario {

    private ControllerFuncionarios controllerFuncionarios;
    private Scanner scan = new Scanner(System.in);

    public TelaCadastroFuncionario(ControllerFuncionarios controllerFuncionarios) {
        this.controllerFuncionarios = controllerFuncionarios;
    }

    public void cadastroFuncionarios() {
        try {
            String input;
            String nome;
            int idade;
            String cpf;
            String endereco;
            String telefone;
            String email;
            String deptAtuacao;
            System.out.println("== Cadastrar Paciente ==");


            System.out.printf("Nome: ");
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            System.out.printf("Endereco: ");
            endereco = scan.nextLine();
            System.out.printf("Telefone: ");
            telefone = scan.nextLine();
            System.out.printf("Email: ");
            email = scan.nextLine();
            System.out.printf("Departamento de atuacao: ");
            deptAtuacao = scan.nextLine();

            String result = controllerFuncionarios.cadastroFuncionarios(nome, idade, cpf, endereco, telefone, email, deptAtuacao);
            System.out.println(result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void queryFucionario() {
        try {
            String cpf;
            System.out.println("== Consulta Funcionario ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerFuncionarios.queryFuncionario(cpf);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void removeFuncionario() {
        try {
            String cpf;
            System.out.println("== Delete Funcionario ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerFuncionarios.deleteFuncionario(cpf);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public int confRemov() {
        int val = 0;
        String mgs = "";
        while (val == 0) {
            System.out.println("DESEJA REALMENTE EXCLUIR ESSE CADASTRO ? (S-sim ou N-nao): ");
            mgs = scan.nextLine();
            if ("s".equalsIgnoreCase(mgs)) {
                return 1;
            } else if ("n".equalsIgnoreCase(mgs)) {
                return 2;
            } else {
                System.out.println("POR FAVOR, DIGITE A OPÇÃO CORRETA! ");
            }
        }
        return -1;
    }

    public String[] changeFuncionario(String nome, int idade, String endereco, String telefone, String email, String deptAtuacao) {
        try {
            String input;
            System.out.println("== Alterar Dados Funcionario ==");
            System.out.printf("Nome: ");
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Endereco: " + endereco);
            endereco = scan.nextLine();
            System.out.printf("Telefone: " + telefone);
            telefone = scan.nextLine();
            System.out.printf("Email: " + email);
            email = scan.nextLine();
            System.out.printf("Departamento de atuacao: ");
            deptAtuacao = scan.nextLine();


            String[] data = {nome, Integer.toString(idade), endereco, telefone, email, deptAtuacao};
            return data;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public void queryChange() {
        try {
            String cpf;
            System.out.println("== Alterar Funcionario ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            String result = controllerFuncionarios.changeFuncionario(cpf);
            System.out.println(result);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void telaFuncionarios() {


        String input;
        int opcao = -1;
        while (opcao != 0) {

            System.out.println("== Cadastro de Funcionarios ==");
            System.out.println("1. Cadastrar Funcionarios.");
            System.out.println("2. Consulta Funcionarios.");
            System.out.println("3. Alterar Funcionarios.");
            System.out.println("4. Exclui Funcionarios.");
            System.out.println("0. Sair");


            System.out.println("Digite a opcao desejada:");
            input = scan.nextLine();
            opcao = Integer.parseInt(input);

            switch (opcao) {
                case 1:
                    cadastroFuncionarios();
                    break;

                case 2:
                    queryFucionario();
                    break;
                case 3:
                    queryChange();
                    break;
                case 4:
                    removeFuncionario();
                    break;


            }
        }
    }
}

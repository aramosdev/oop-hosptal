/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerMedico;
import java.util.Scanner;

/**
 *
 * @author aramos
 */
public class TelaCadastroMedico {
    // TODO code application logic here

    private ControllerMedico controllerMedico;
    private Scanner scan = new Scanner(System.in);

    public TelaCadastroMedico(ControllerMedico controllerMedico) {
        this.controllerMedico = controllerMedico;
    }

    public void cadastroMedico() {

        try {
            String input;
            String crm;
            String nome;
            int idade;
            String cpf;
            String endereco;
            String telefone;
            String email;
            System.out.println("== Cadastrar Médico ==");
            System.out.printf("CRM: ");
            crm = scan.nextLine();
            System.out.printf("Nome: ");
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            System.out.printf("Endereco: ");
            endereco = scan.nextLine();
            System.out.printf("Telefone: ");
            telefone = scan.nextLine();
            System.out.printf("Email: ");
            email = scan.nextLine();

            String resurt = controllerMedico.cadastroMedico(crm, nome, idade, cpf, endereco, telefone, email);
            System.out.println(resurt);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void queryMedico() {
        try {
            String cpf;
            System.out.println("== Consulta Medico ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerMedico.consultCpfMedico(cpf);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void removeMedico() {
        try {
            String cpf;
            System.out.println("== Delete Medico ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerMedico.deleteMedico(cpf);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public int confRemov() {
        int val = 0;
        String mgs = "";
        while (val == 0) {
            System.out.println("DESEJA REALMENTE EXCLUIR ESSE CADASTRO ? (S-sim ou N-nao): ");
            mgs = scan.nextLine();
            if ("s".equalsIgnoreCase(mgs)) {
                return 1;
            } else if ("n".equalsIgnoreCase(mgs)) {
                return 2;
            } else {
                System.out.println("POR FAVOR, DIGITE A OPÇÃO CORRETA! ");
            }
        }
        return -1;
    }
    
    public void queryChange() {
        try {
            String cpf;
            System.out.println("== Alterar Medico ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            String result = controllerMedico.changeMedico(cpf);
            System.out.println(result);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    public String[] changeMedico(String nome, int idade, String endereco, String telefone, String email) {
        try {
            String input;
            System.out.println("== Alterar Dados Medico ==");
            System.out.printf("Nome: " );
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Endereco: " + endereco);
            endereco = scan.nextLine();
            System.out.printf("Telefone: " + telefone);
            telefone = scan.nextLine();
            System.out.printf("Email: " + email);
            email = scan.nextLine();


            String [] data = {nome,Integer.toString(idade), endereco, telefone, email};
             return data;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
       return null;
    }

    public void telaMedico() {

        String input;
        int opcao = -1;
        while (opcao != 0) {
            try {
                System.out.println("== Cadastro de Medico ==");
                System.out.println("1. Cadastrar Medico.");
                System.out.println("2. Consulta Medico.");
                System.out.println("3. Alterar Medico.");
                System.out.println("4. Exclui Medico");
                System.out.println("0. Voltar");


                System.out.println("Digite a opcao desejada:");

                input = scan.nextLine();
                opcao = Integer.parseInt(input);

            } catch (Exception e) {
                System.err.println(e.getMessage());
            }

            switch (opcao) {
                case 1:
                    cadastroMedico();
                    break;
                case 2:
                    queryMedico();
                    break;
                case 3:
                    queryChange();
                    break;
                case 4:
                    removeMedico();
                    break;
            }
        }
    }
}

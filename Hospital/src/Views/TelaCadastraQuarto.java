/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerQuarto;
import java.util.Scanner;

/**
 *
 * @author aramos
 */
public class TelaCadastraQuarto {

    private ControllerQuarto controllerQuarto;
    private Scanner scan = new Scanner(System.in);

    public TelaCadastraQuarto(ControllerQuarto controllerQuarto) {
        this.controllerQuarto = controllerQuarto;
    }

    public void cadasdastroQuarto() {
        try {
            int id;
            int numberMaxLeitos;
            String input;
            System.out.println("== Cadastrar Quarto ==");
            System.out.printf("Codigo do quarto: ");
            input = scan.nextLine();
            id = Integer.parseInt(input);
            System.out.printf("Numero Maximo de leitos: ");
            input = scan.nextLine();
            numberMaxLeitos = Integer.parseInt(input);

            String result = controllerQuarto.saveQuarto(id, numberMaxLeitos);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void cadastroLeito() {
        try {
            String cod;
            int id;
            String input;
            System.out.println("== Cadastrar Leito ==");
            System.out.printf("Codigo do Leito: ");
            cod = scan.nextLine();
            System.out.printf("Codigo do Quarto: ");
            input = scan.nextLine();
            id = Integer.parseInt(input);
            
            String result = controllerQuarto.saveLeito(cod, id);
            System.out.println(result);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void telaQuarto() {

        String input;
        int opcao = -1;
        while (opcao != 0) {
            try {
                System.out.println("== Cadastro de Quarto ==");
                System.out.println("1. Cadastrar Quarto.");
                System.out.println("2. Cadastrar Leito.");
                System.out.println("3. Alterar Medico.");
                System.out.println("4. Exclui Medico");
                System.out.println("0. Voltar");


                System.out.println("Digite a opcao desejada:");

                input = scan.nextLine();
                opcao = Integer.parseInt(input);

            } catch (Exception e) {
                System.err.println(e.getMessage());
            }

            switch (opcao) {
                case 1:
                    cadasdastroQuarto();
                    break;
                case 2:
                    cadastroLeito();
                    break;
                case 3:
//                    queryChange();
                    break;
                case 4:
//                    removeMedico();
                    break;
            }
        }
    }
}

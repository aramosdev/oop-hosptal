/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerPaciente;
import java.util.Scanner;

/**
 *
 * @author alberto
 */
public class TelaCadastroPaciente {

    private ControllerPaciente controllerPaciente;
    private Scanner scan = new Scanner(System.in);

    public TelaCadastroPaciente(ControllerPaciente controllerPaciente) {
        this.controllerPaciente = controllerPaciente;
    }

    public void cadastroPaciente() {

        try {
            String input;
            int id;
            String nome;
            int idade;
            String cpf;
            String endereco;
            String telefone;
            String email;
            String dataInternacao;
            System.out.println("== Cadastrar Paciente ==");
            System.out.printf("Id: ");
            input = scan.nextLine();
            id = Integer.parseInt(input);
            System.out.printf("Nome: ");
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            System.out.printf("Endereco: ");
            endereco = scan.nextLine();
            System.out.printf("Telefone: ");
            telefone = scan.nextLine();
            System.out.printf("Email: ");
            email = scan.nextLine();
            System.out.printf("Data de Internação: ");
            dataInternacao = scan.nextLine();

            String result = controllerPaciente.cadastroPaciente(id, nome, idade, cpf, endereco, telefone, email, dataInternacao);
            System.out.println(result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void queryPaciente() {
        try {
            String cpf;
            System.out.println("== Consulta Paciente ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerPaciente.consultPaciente(cpf);
            System.out.println(result);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public int confRemov() {
        int val = 0;
        String mgs = "";
        while (val == 0) {
            System.out.println("DESEJA REALMENTE EXCLUIR ESSE CADASTRO ? (S-sim ou N-nao): ");
            mgs = scan.nextLine();
            if ("s".equalsIgnoreCase(mgs)) {
                return 1;
            } else if ("n".equalsIgnoreCase(mgs)) {
                return 2;
            } else {
                System.out.println("POR FAVOR, DIGITE A OPÇÃO CORRETA! ");
            }
        }
        return -1;
    }

    public void removePaciente() {
        try {
            String cpf;
            System.out.println("== Delete Paciente ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();

            String result = controllerPaciente.deletePaciente(cpf);
            System.out.println(result);



        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void queryChange() {
        try {
            String cpf;
            System.out.println("== Alterar Paciente ==");
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            String result = controllerPaciente.changePaciente(cpf);
            System.out.println(result);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public String[] changePaciente(String nome, int idade, String endereco, String telefone, String email, String dataInternacao) {
        try {
            String input;
            System.out.println("== Alterar Dados Paciente ==");
            System.out.printf("Nome: " );
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Endereco: " + endereco);
            endereco = scan.nextLine();
            System.out.printf("Telefone: " + telefone);
            telefone = scan.nextLine();
            System.out.printf("Email: " + email);
            email = scan.nextLine();
            System.out.printf("Data de Internação: " + dataInternacao);
            dataInternacao = scan.nextLine();

            String [] data = {nome,Integer.toString(idade), endereco, telefone, email, dataInternacao};
             return data;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
       return null;
    }

    public void TelaPaciente() {

        String input;
        int opcao = -1;
        while (opcao != 0) {
            try {
                System.out.println("== Cadastro de Paciente ==");
                System.out.println("1. Cadastrar Paciente.");
                System.out.println("2. Consulta Paciente.");
                System.out.println("3. Alterar Paciente.");
                System.out.println("4. Exclui Paciente");
                System.out.println("0. Voltar");


                System.out.println("Digite a opcao desejada:");

                input = scan.nextLine();
                opcao = Integer.parseInt(input);

            } catch (Exception e) {
                System.err.println(e.getMessage());
            }

            switch (opcao) {
                case 1:
                    cadastroPaciente();
                    break;
                case 2:
                    queryPaciente();
                    break;
                case 3:
                    queryChange();
                    break;
                case 4:
                    removePaciente();
                    break;
            }
        }
    }
}

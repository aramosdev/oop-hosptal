/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciarFuncionarios;
import Views.TelaCadastroFuncionario;
import Model.Pessoa.Funcionarios;

/**
 *
 * @author alberto
 */
public class ControllerFuncionarios {

    private TelaCadastroFuncionario cadastroFuncionario;
    private GerenciarFuncionarios cadFuncionarios;

    public ControllerFuncionarios() {
        this.cadastroFuncionario = new TelaCadastroFuncionario(this);
        this.cadastroFuncionario.telaFuncionarios();
        
    }

    public String cadastroFuncionarios(String nome, int idade, String cpf,
            String endereço, String telefone, String email, String deptAtuacao) {
        
        cadFuncionarios = new GerenciarFuncionarios();
        Funcionarios funcionario = new Funcionarios(deptAtuacao, nome, idade, cpf, endereço, telefone, email);
        return cadFuncionarios.cadastroFuncionarios(funcionario);

    }
    
    public String queryFuncionario(String _cpf){
        cadFuncionarios = new GerenciarFuncionarios();
        int index = cadFuncionarios.queryFuncionario(_cpf);
        if(index == -1)return "FUNCIONARIO NÃO CONSTA NO SISTEMA!";
        return cadFuncionarios.getFuncionarios().get(index).toString();
    }
    
    public String deleteFuncionario(String _cpf) {
        cadFuncionarios = new GerenciarFuncionarios();
        int index =cadFuncionarios.queryFuncionario(_cpf);
        if (index == -1) return "FUNCIONARIO NÃO CONSTA NO SITEMA";
        Funcionarios f = cadFuncionarios.getFuncionarios().get(index);
        int conf = cadastroFuncionario.confRemov();
        if (conf != 1) {
            return "OPERAÇÃO CANCELADA";
        } else {
            return cadFuncionarios.deleteFuncionario(f);
        }
    }
    
    public String changeFuncionario(String _cpf) {
        cadFuncionarios = new GerenciarFuncionarios();
        int index =cadFuncionarios.queryFuncionario(_cpf);
        if (index == -1) return "FUNCIONARIO NÃO CONSTA NO SITEMA";
        Funcionarios f = cadFuncionarios.getFuncionarios().get(index);
        String nome = f.getNome();
        int idade = f.getIdade();
        String cpf = f.getCpf();
        String endereco = f.getEndereco();
        String telefone = f.getTelefone();
        String email = f.getEmail();
        String depAtuacao = f.getDeptAtuacao();
        
        String[] data = cadastroFuncionario.changeFuncionario(nome, idade, endereco, telefone, email, depAtuacao);
        
        f.setNome(data[0]);
        f.setIdade(Integer.parseInt(data[1]));
        f.setEndereço(data[2]);
        f.setTelefone(data[3]);
        f.setEmail(data[4]);
        f.setDeptAtuacao(data[5]);
        
        
        return cadFuncionarios.changeFuncionario(f);
    }
}

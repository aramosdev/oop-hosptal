/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Agenda.Agendamento;
import Views.TelaAgenda;


/**
 *
 * @author aramos
 */
public class ControllerAgenda {

    private TelaAgenda telaAgenda;
    
    public ControllerAgenda() {
        this.telaAgenda= new TelaAgenda(this);
        this.telaAgenda.telaAgenda();
    }
    public String agendarConsulta(int dia, int mes, int periodo,String cpfMedico, String cpfPaciente ){
        Agendamento agenda = new Agendamento();
        return agenda.agendaConsulta(dia, mes, cpfMedico, cpfPaciente, periodo);
    }
    
    public String deleteConsult(int _dia, String _type){
        Agendamento agenda = new Agendamento();
        boolean status = agenda.deleteConsult(_dia, _type);
        if (status == false)return "NÃO CONSTA CONSULTA NESSA DATA!";
            else return "CONSULTA EXCLUIDA COM SUCESSO!";
        
    }
       
       
}

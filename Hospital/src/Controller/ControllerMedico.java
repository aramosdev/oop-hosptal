/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciarMedico;
import Model.Pessoa.Medico;
import Views.TelaCadastroMedico;

/**
 *
 * @author aramos
 */
public class ControllerMedico {

    private TelaCadastroMedico cadastroMedico;
    private GerenciarMedico cadMedico;
    
    public ControllerMedico() {
        this.cadastroMedico = new TelaCadastroMedico(this);
        this.cadastroMedico.telaMedico();
        
    }
    public String cadastroMedico(String crm, String nome, int idade, String cpf,
            String endereço, String telefone, String email){
        cadMedico = new GerenciarMedico();
        Medico medico = new Medico(crm, nome, idade, cpf, endereço, telefone, email);
        return cadMedico.cadastroMedico(medico);
        
    }
    public String consultCpfMedico(String _cpf){
        cadMedico = new GerenciarMedico();
        int index = cadMedico.consultMedicoCpf(_cpf);
        if(index == -1)return "MEDICO NÃO CONSTA NO SISTEMA!";
        return cadMedico.getMedicos().get(index).toString(); 
    }
    
    public String deleteMedico(String _cpf) {
        cadMedico = new GerenciarMedico();
        int index =cadMedico.consultMedicoCpf(_cpf);
        if (index == -1) return "PACIENTE NÃO CONSTA NO SITEMA";
        Medico m = cadMedico.getMedicos().get(index);
        int conf = cadastroMedico.confRemov();
        if (conf != 1) {
            return "OPERAÇÃO CANCELADA";
        } else {
            return cadMedico.deleteMedico(m);
        }
    }
    
    public String changeMedico(String _cpf) {
        cadMedico = new GerenciarMedico();
        int index =cadMedico.consultMedicoCpf(_cpf);
        if (index == -1) return "PACIENTE NÃO CONSTA NO SITEMA";
        Medico m = cadMedico.getMedicos().get(index);
        String nome = m.getNome();
        int idade = m.getIdade();
        String cpf = m.getCpf();
        String endereco = m.getEndereco();
        String telefone = m.getTelefone();
        String email = m.getEmail();
        
        String[] data = cadastroMedico.changeMedico(nome, idade, endereco, telefone, email);
        
        m.setNome(data[0]);
        m.setIdade(Integer.parseInt(data[1]));
        m.setEndereço(data[2]);
        m.setTelefone(data[3]);
        m.setEmail(data[4]);
        
        
        return cadMedico.changeMedico(m);
    }
}

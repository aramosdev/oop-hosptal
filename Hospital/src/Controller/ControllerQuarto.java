/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Leito.GerenciaQuarto;
import Model.Leito.Leito;
import Model.Leito.Quarto;
import Views.TelaCadastraQuarto;

/**
 *
 * @author aramos
 */
public class ControllerQuarto {
    
    private TelaCadastraQuarto cadastraQuarto;
    private GerenciaQuarto gerenciaQuarto;
    
    public ControllerQuarto() {
        this.cadastraQuarto = new TelaCadastraQuarto(this);
        this.cadastraQuarto.telaQuarto();
    }
    
    public String saveQuarto(int id, int numberMaxLeitos){
        gerenciaQuarto = new GerenciaQuarto();
        Quarto quarto = new Quarto(id, numberMaxLeitos);
        
        return gerenciaQuarto.saveQuarto(quarto);
    }
    
    public String saveLeito(String _cod, int id){
        gerenciaQuarto = new GerenciaQuarto();
        Leito leito = new Leito(_cod);
        return gerenciaQuarto.saveLeito(leito, id);
    }
    
}

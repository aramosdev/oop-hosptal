/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciaPaciente;
import Model.Pessoa.Paciente;
import Views.TelaCadastroPaciente;

/**
 *
 * @author alberto
 */
public class ControllerPaciente {

    private TelaCadastroPaciente cadastroPaciente;
    private GerenciaPaciente cadPaciente;

    public ControllerPaciente() {
        this.cadastroPaciente = new TelaCadastroPaciente(this);
        this.cadastroPaciente.TelaPaciente();

    }

    public String cadastroPaciente(int id, String nome, int idade, String cpf,
            String endereço, String telefone, String email, String dataInternacao) {

        Paciente paciente = new Paciente(id, dataInternacao, nome, idade, cpf, endereço, telefone, email);

        cadPaciente = new GerenciaPaciente();

        return cadPaciente.cadastroPaciente(paciente);
    }

    public String consultPaciente(String _cpf) {
        cadPaciente = new GerenciaPaciente();
        Paciente p = cadPaciente.consultPaciente(_cpf);
        if (p == null) {
            return "PACIENTE NÃO CONSTA NO SITEMA";
        } else {
            return p.toString();

        }
    }

    public String deletePaciente(String _cpf) {
        cadPaciente = new GerenciaPaciente();
        Paciente p = cadPaciente.consultPaciente(_cpf);
        if (p == null) {
            return "PACIENTE NÃO CONSTA NO SITEMA";
        }
        int conf = cadastroPaciente.confRemov();
        if (conf != 1) {
            return "OPERAÇÃO CANCELADA";
        } else {
            return cadPaciente.deletePaciente(p);
        }
    }

    public String changePaciente(String _cpf) {
        cadPaciente = new GerenciaPaciente();
        Paciente p = cadPaciente.consultPaciente(_cpf);
        if (p == null) return "PACIENTE NÃO CONSTA NO SITEMA";
        String nome = p.getNome();
        int idade = p.getIdade();
        String cpf = p.getCpf();
        String endereco = p.getEndereco();
        String telefone = p.getTelefone();
        String email = p.getEmail();
        String dataInternacao = p.getDataInternacao();
        
        String[] data = cadastroPaciente.changePaciente(nome, idade, endereco, telefone, email, dataInternacao);
        
        p.setNome(data[0]);
        p.setIdade(Integer.parseInt(data[1]));
        p.setEndereço(data[2]);
        p.setTelefone(data[3]);
        p.setEmail(data[4]);
        p.setDataInternacao(data[5]);
        
        return cadPaciente.changePaciente(p);
    }
}
